package imagedots.core;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageConverter {
	
	private ImageConverter() {
		// Do not allow instances.
	}

	/*
	 *  Creates a Graphics object from an input image file
	 */
	public static Graphics2D convertFileToImage(File imageFile) {
		BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageIO.read(imageFile);
        } catch (IOException e) {
              System.out.println("Exception occured :" + e.getMessage());
        }
        return bufferedImage.createGraphics();
   }
}
