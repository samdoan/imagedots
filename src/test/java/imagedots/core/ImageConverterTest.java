package imagedots.core;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.awt.Graphics2D;
import java.io.File;

import org.junit.jupiter.api.Test;

public class ImageConverterTest {

	private static final String VALID_IMAGE_FILE = "urban_rooftop_view.jpeg";
	
	@Test
	public void convertFileToImageSuccessful() {
		File fileImage = new File("src/test/resources/images/" + VALID_IMAGE_FILE);
		Graphics2D graphicsImage = ImageConverter.convertFileToImage(fileImage);
		assertNotNull(graphicsImage);
	}
}
